#Twig template engine in Nooku

This is just a proof of concept at the moment.

Basic usage as follows:

Place the following in your default.php (or whatever layout you want to use twig in)

	<ktml:twig />

and attach the filter to the template, 1 of 2 ways:

Through your view:

	<?php

    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'template_filters' => array('com://site/twig.template.filter.twig')
        ));
        parent::_initialize($config);
    }

Or in the corresponding template:

	<?php

    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'filters' => array('com://site/twig.template.filter.twig')
        ));
        parent::_initialize($config);
    }

Your twig template will then be injected in place of the `<ktml:twig />` placeholder