<?php
/**
 * User: oli griffiths
 * Date: 2/4/13
 * Time: 9:54 AM
 */

class ComTwigTemplateFilterTwig extends KTemplateFilterAbstract implements KTemplateFilterWrite
{
    /**
     * Constructor.
     *
     * @param   object  An optional KConfig object with configuration options
     */
    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        //Load twig autoloader
        require_once dirname(dirname(dirname(__FILE__))) . '/lib/Twig/Autoloader.php';
        Twig_Autoloader::register();
    }


    /**
     * Ensure filter runs first
     * @param KConfig $config
     */
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'priority' => KCommand::PRIORITY_HIGHEST,
        ));

        parent::_initialize($config);
    }


    /**
     * Parse out <ktml:twig /> tags and replace with twig content
     *
     * @param string Block of text to parse
     * @return KTemplateFilterWrite
     */
    public function write(&$text)
    {
        if(strpos($text,'<ktml:twig />') !== false){
            //Find file and path
            $file       = $this->getTemplate()->findFile($this->getTemplate()->getPath());
            $file       = preg_replace('#\.php$#','.html',$file);
            $path       = dirname($file);
            $file       = basename($file);

            //Instantiate twig
            $loader     = new Twig_Loader_Filesystem($path);
            $twig       = new Twig_Environment($loader);

            //Render
            $content    = $twig->render($file, $this->getTemplate()->getData());

            //Replace tag
            $text       = str_replace('<ktml:twig />', $content, $text);
        }
    }
}